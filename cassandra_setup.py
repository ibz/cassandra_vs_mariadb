"""
Python  by Techfossguru
Copyright (C) 2017  Satish Prasad

"""
import logging
from cassandra.cluster import Cluster, BatchStatement
import string
import random
from random import randint

primary_key = 1
hobbies = []
hobbies.append('Golf')
hobbies.append('Schwimmen')
hobbies.append('Datenbanken')


def randomword(length):
    return ''.join(random.choice(
        string.ascii_lowercase) for i in range(length))


def create_batch(insert_sql):
    global primary_key
    counter = 1
    batch = BatchStatement()
    while 15 > counter:
        param_counter = 0
        params = []
        while 13 > param_counter:
            name = randomword(6)
            params.append(name)
            param_counter += 1

        batch.add(insert_sql, (primary_key, params[0], params[1], params[2],
                               params[3], params[4], params[5], params[6],
                               params[7], hobbies[randint(0, 2)], params[9],
                               params[10], params[11], params[12]))
        counter += 1
        primary_key += 1
    return batch


class PythonCassandraExample:

    def __init__(self):
        self.cluster = None
        self.session = None
        self.keyspace = None
        self.log = None

    def __del__(self):
        self.cluster.shutdown()

    def createsession(self):
        self.cluster = Cluster(['localhost'])
        self.session = self.cluster.connect(self.keyspace)

    def getsession(self):
        return self.session

    # How about Adding some log info to see what went wrong
    def setlogger(self):
        log = logging.getLogger()
        log.setLevel('INFO')
        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter(
            "%(asctime)s [%(levelname)s] %(name)s: %(message)s"))
        log.addHandler(handler)
        self.log = log

    # Create Keyspace based on Given Name
    def createkeyspace(self, keyspace):
        """
        :param keyspace:  The Name of Keyspace to be created
        :return:
        """
        # Before we create new lets check if exiting keyspace; we will drop
        # that and create new
        rows = self.session.execute(
            "SELECT keyspace_name FROM system_schema.keyspaces")
        if keyspace in [row[0] for row in rows]:
            self.log.info("dropping existing keyspace...")
            self.session.execute("DROP KEYSPACE " + keyspace)

        self.log.info("creating keyspace...")
        self.session.execute("""
                CREATE KEYSPACE %s
                WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '2' }
                """ % keyspace)

        self.log.info("setting keyspace...")
        self.session.set_keyspace(keyspace)

    def create_table(self):
        c_sql = """
                CREATE TABLE IF NOT EXISTS user (id int PRIMARY KEY,
                                              username varchar,
                                              email varchar,
                                              first_name varchar,
                                              last_name varchar,
                                              middle_name varchar,
                                              street_name varchar,
                                              telephonenumber varchar,
                                              mobile_number varchar,
                                              hobby varchar,
                                              salutation varchar,
                                              gender varchar,
                                              city varchar,
                                              country varchar);
                 """
        self.session.execute(c_sql)
        self.log.info("User Table Created !!!")

    def insert_data(self):
        insert_sql = self.session.prepare(
            """
            INSERT INTO user (id,
                              username,
                              email,
                              first_name,
                              last_name,
                              middle_name,
                              street_name,
                              telephonenumber,
                              mobile_number,
                              hobby,
                              salutation,
                              gender,
                              city,
                              country) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)
            """
        )
        counter = 1
        while 100000 > counter:
            batch = create_batch(insert_sql)
            self.session.execute(batch)
            counter += 1
        self.log.info('Batch Insert Completed')


# lets do some batch insert
if __name__ == '__main__':
    example1 = PythonCassandraExample()
    example1.createsession()
    example1.setlogger()
    example1.createkeyspace('userdb')
    example1.create_table()
    example1.insert_data()
    #example1.select_data()
