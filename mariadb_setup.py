#!/usr/bin/python
import mysql.connector as mariadb
import string
import random
from random import randint

hobbies = []
hobbies.append('Golf')
hobbies.append('Schwimmen')
hobbies.append('Datenbanken')


def randomword(length):
    return ''.join(random.choice(
        string.ascii_lowercase) for i in range(length))


def create_data():

    mariadb_connection = mariadb.connect(user='vagrant',
                                         database='testdb')
    cursor = mariadb_connection.cursor()
    cursor.execute("""create table if not exists user (
            id bigint auto_increment primary key,
            username varchar(128),
            first_name varchar(128),
            middle_name varchar(128),
            last_name varchar(128),
            street_name varchar(128),
            city_name varchar(128),
            country_name varchar(128),
            telephonenumber varchar(128),
            mobile_number varchar(128),
            email varchar(128),
            hobby varchar(128),
            gender varchar(128),
            salutation varchar(128));
    """)
    counter = 0
    while 100000 > counter:
        inner_counter = 0
        while 15 > inner_counter:
            param_counter = 0
            params = []
            while 13 > param_counter:
                name = randomword(6)
                params.append(name)
                param_counter += 1

            cursor.execute("""INSERT INTO user (
                username,
                first_name,
                middle_name,
                last_name,
                street_name,
                city_name,
                country_name,
                telephonenumber,
                mobile_number,
                email,
                hobby,
                gender,
                salutation)
                VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);""",
                           (params[0], params[1], params[2], params[3],
                            params[4], params[5], params[6], params[7],
                            params[8], params[9], hobbies[randint(0, 2)],
                            params[10], params[11]))
            inner_counter += 1
        mariadb_connection.commit()
        counter += 1


create_data()
