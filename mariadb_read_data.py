#!/usr/bin/python
import mysql.connector as mariadb
import time

mariadb_connection = mariadb.connect(user='vagrant',
                                     database='testdb')
cursor = mariadb_connection.cursor()

allowed_query = ("select username, hobby from user;")

print()
print()
print()
print()
print()
start_time = time.time()
cursor.execute(allowed_query)
data = cursor.fetchall()
print("--- %s seconds ---" % (time.time() - start_time))
print(len(data))
print()
print()
print()
print()
print()

