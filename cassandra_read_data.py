"""
Python  by Techfossguru
Copyright (C) 2017  Satish Prasad

"""
import logging
from cassandra.cluster import Cluster
import time


class PythonCassandraExample:

    def __init__(self):
        self.cluster = None
        self.session = None
        self.keyspace = 'userdb'
        self.log = None

    def __del__(self):
        self.cluster.shutdown()

    def createsession(self):
        self.cluster = Cluster(['localhost'])
        self.session = self.cluster.connect(self.keyspace)

    def getsession(self):
        return self.session

    # How about Adding some log info to see what went wrong
    def setlogger(self):
        log = logging.getLogger()
        log.setLevel('INFO')
        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter(
            "%(asctime)s [%(levelname)s] %(name)s: %(message)s"))
        log.addHandler(handler)
        self.log = log

    def select_data(self):
        evil_query = (
            "select username, hobby from user where hobby='Datenbanken';")
        allowed_query = ("select username, hobby from user;")
        print()
        print()
        print()
        print()
        print()
        start_time = time.time()
        rows = self.session.execute(allowed_query)
        print("--- %s seconds ---" % (time.time() - start_time))
        counter = 1
        for row in rows:
            counter += 1
        print(counter)
        print()
        print()
        print()
        print()
        print(evil_query)
        print()
        try:
            rows = self.session.execute(evil_query)
        except Exception as e:
            print(e.args)

    def update_data(self):
        pass

    def delete_data(self):
        pass


if __name__ == '__main__':
    example1 = PythonCassandraExample()
    example1.createsession()
    example1.setlogger()
    example1.select_data()
