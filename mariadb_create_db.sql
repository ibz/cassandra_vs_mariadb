-- Creates a test DB
--
-- Author: Andreas Zweili
-- 2018-09-02

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

create database if not exists testdb;
flush privileges;
ALTER DATABASE `testdb` CHARACTER SET utf8;

create user 'vagrant'@'localhost';
GRANT ALL ON *.* TO 'vagrant'@'localhost';
